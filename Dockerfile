FROM debian:9 as build

RUN apt update && apt install -y wget gcc make libpcre3 libpcre3-dev zlib1g zlib1g-dev
RUN wget http://nginx.org/download/nginx-1.22.1.tar.gz && tar xvfz nginx-1.22.1.tar.gz && cd nginx-1.22.1 && ./configure && make && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/ /usr/local/nginx/
RUN chmod +x nginx
CMD ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]